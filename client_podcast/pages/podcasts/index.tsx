import Card from "../../components/Card";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

type Podcast = {
  id: string;
  attributes: {
    title: string;
    description: string;
  };
};

export default function Podcasts() {
  const router = useRouter();
  const { id } = router.query;

  const [datas, setData] = useState<Podcast[]>([]);


  useEffect(() => {
    fetch('http://localhost:1337/api/podcasts/')
      .then(response => response.json())
      .then(data => {
        setData(data.data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  console.log(datas);



  return (
    <>
      <h1 className="text-3xl font-black text-center">Tous les Podcasts</h1>
      <div className="flex justify-around py-5">
        {datas?.map((data) =>
          <Card title={data.attributes.title} description={data.attributes.description} id={data.id} key={data.id} />
        )}
      </div>
    </>
  );
}