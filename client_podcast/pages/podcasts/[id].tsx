import { useRouter } from "next/router";

export default function PodcastPage() {
  const router = useRouter();
  const { id } = router.query;

  return <p>Podcast {id}</p>;
}